<?php

namespace app\controllers;

use app\models\Config;
use app\models\Posto;
use Yii;
use app\models\PrecoCombustivel;
use yii\data\ActiveDataProvider;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\rest\ActiveController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
/**
 * PrecoController implements the CRUD actions for PrecoCombustivel model.
 */
class PrecoController extends ActiveController
{
    public $modelClass = 'app\models\PrecoCombustivel';


    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to

                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['*'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Headers' => ['*'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],

            ],
        ];
    }
    public function actions()
    {
        $actions = parent::actions();

        unset($actions['create']);
        unset($actions['index']);
        return $actions;
    }

    public function actionCreate(){
        if (Yii::$app->request->post()) {
            $posto = (Yii::$app->request->post());;
            $preco = new PrecoCombustivel();
            $preco->preco = $posto['posto']['_valor'];
            $preco->data = Yii::$app->formatter->asDate('now', 'php:Y-m-d H:i:s');
            $preco->id_posto = $posto['posto']['_id'];
            $preco->save();
            echo Json::encode($preco);
        }
    }

    public function actionIndex(){
        if(Yii::$app->request->get()){
            $get = Yii::$app->request->get();
            $preco = PrecoCombustivel::find()
                ->where(['id_posto' => $get['posto_id']])
                ->orderBy('id')
                ->one();

            echo Json::encode($preco);

        }

    }

    public function actionBetterprice(){
        $postos=[];
        if(Yii::$app->request->post()){
            $post = Yii::$app->request->post();
            $coordinates = Json::decode($post['coordinates']);
            foreach ($coordinates as $coordinate){
                $coordinate = Json::decode($coordinate);
                $client = new Client(['baseUrl' => 'https://maps.googleapis.com/maps/api/place/nearbysearch/json']);
                $gas_stations = $client->get('', [
                        'location' => $coordinate['latitude'].','.$coordinate['longitude'],
                        'radius'=>'1000',
                        'type'=>'gas_station',
                        'key'=>Config::ApiKey()
                    ]
                )->send();


                foreach($gas_stations->getData()["results"] as $gas_station){
                    if(Posto::find()->where(['place_id'=>$gas_station["id"]])->exists()){
                        $posto = Posto::findOne(['place_id'=>$gas_station["id"]]);
                        $exists = 0;

                            foreach ($postos as $p){
                                if($posto->id==$p->id)
                                    $exists++;
                            }

                        if(count ($posto->precoCombustivels)>0 && $exists<=0){

                            $postos[]=[
                                'latitude'=>$posto->latitude,
                                'longitude'=>$posto->longitude,
                                'valor'=>$posto->precoCombustivels[0]->preco
                            ];
                        }
                    }
                }

            }
            echo Json::encode($postos);

        }
    }


}
