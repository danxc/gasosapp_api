<?php

namespace app\controllers;

use app\models\Config;
use app\models\PrecoCombustivel;
use Yii;
use app\models\Posto;
use app\models\PostoSearch;
use yii\rest\ActiveController;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
/**
 * PostoController implements the CRUD actions for Posto model.
 */
class PostoController extends ActiveController
{
    public $modelClass = 'app\models\Posto';

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index']);
        return $actions;
    }

    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['*'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Headers' => ['*'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],

            ],
        ];
    }

    public function actionIndex()
    {
        if(isset($_GET['location'])){
            $postos = array();
            $radius='';
            if(isset($_GET['distance'])) {
                $radius = $_GET['distance'];
                $radius.='000';
            }
            else{
                $radius = '1000';
            }
            $client = new Client(['baseUrl' => 'https://maps.googleapis.com/maps/api/place/nearbysearch/json']);
            $gas_stations = $client->get('', [
                'location' => $_GET['location'],
                'radius'=>$radius,
                'type'=>'gas_station',
                'key'=>Config::ApiKey()
            ]
            )->send();
            foreach ($gas_stations->getData()["results"] as $gas_station){
                $model =  new Posto();
                $model->latitude = strval($gas_station["geometry"]["location"]["lat"]);
                $model->longitude = strval($gas_station["geometry"]["location"]["lng"]);
                $model->place_id = $gas_station["id"];
                $model->nome = $gas_station['name'];
                $model->endereco = $gas_station['vicinity'];
                if(!Posto::find()->where(['place_id'=>$model->place_id])->exists()){
                    $model->save();
                }
                else{
                    $model = Posto::findOne(['place_id'=>$model->place_id]);
                }

                $postos[]=$model->attributes;
            }

            for($i=0; $i<count($postos);$i++){
                $preco = PrecoCombustivel::find()
                    ->where(['id_posto' => $postos[$i]['id']])
                    ->orderBy('data desc')
                    ->one();
                $postos[$i]['valor'] = isset($preco)?$preco->preco:0;
                $postos[$i]['data_registro'] = isset($preco)?$preco->data:"Sem registro";
                $postos[$i]['data_registro_formatada'] = $postos[$i]['data_registro'];
                if(isset($preco)){
                    $postos[$i]['data_registro_formatada'] = Yii::$app->formatter->format($postos[$i]['data_registro'],'relativeTime');
                }

            }
            echo Json::encode($postos);
        }
    }
}
