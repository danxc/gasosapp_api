<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "preco_combustivel".
 *
 * @property integer $id
 * @property double $preco
 * @property string $data
 * @property integer $id_posto
 *
 * @property Posto $idPosto
 */
class PrecoCombustivel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'preco_combustivel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['preco', 'data', 'id_posto'], 'required'],
            [['preco'], 'number'],
            [['id_posto'], 'integer'],
            [['data'], 'string', 'max' => 45],
            [['id_posto'], 'exist', 'skipOnError' => true, 'targetClass' => Posto::className(), 'targetAttribute' => ['id_posto' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'preco' => 'Preco',
            'data' => 'Data',
            'id_posto' => 'Id Posto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPosto()
    {
        return $this->hasOne(Posto::className(), ['id' => 'id_posto']);
    }
}
