<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "posto".
 *
 * @property integer $id
 * @property string $nome
 * @property string $endereco
 * @property string $latitude
 * @property string $longitude
 * @property string $place_id
 *
 * @property PrecoCombustivel[] $precoCombustivels
 */
class Posto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nome', 'endereco', 'latitude', 'longitude','place_id'], 'required'],
            [['nome', 'latitude', 'longitude','place_id'], 'string', 'max' => 45],
            [['endereco'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'endereco' => 'Endereco',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'place_id'=>"ID Google",
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrecoCombustivels()
    {
        return $this->hasMany(PrecoCombustivel::className(), ['id_posto' => 'id']);
    }
}
