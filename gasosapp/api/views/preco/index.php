<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Preco Combustivels';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="preco-combustivel-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Preco Combustivel', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'preco',
            'data',
            'id_posto',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
