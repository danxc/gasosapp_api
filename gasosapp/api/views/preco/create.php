<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PrecoCombustivel */

$this->title = 'Create Preco Combustivel';
$this->params['breadcrumbs'][] = ['label' => 'Preco Combustivels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="preco-combustivel-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
